import {GameScene} from './scenes/GameScene.js'

// create a new scene named "Game"
let gameScene = new GameScene();
 
// our game's configuration
let config = {
  type: Phaser.AUTO,  //Phaser will decide how to render our game (WebGL or Canvas)
  width: 640, // game width
  height: 360, // game height
  scene: gameScene // our newly created scene
};
 
// create the game, and pass it the configuration
let game = new Phaser.Game(config);

//Add the game to Window so we can access it from the console.
Window.Game = game;