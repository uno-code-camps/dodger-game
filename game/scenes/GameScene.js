export class GameScene extends Phaser.Scene {
    constructor() {
        super('MyGameScene')
    }

    init(){
        this.playerSpeed = 1.5;
        this.enemyMaxY = 280;
        this.enemyMinY = 80;
    }

    preload(){
        //load images
        this.load.image('background', '/game/assets/background.png');
        this.load.image('player', '/game/assets/player.png');
        this.load.image('dragon', '/game/assets/dragon.png');   
        this.load.image('treasure', '/game/assets/treasure.png');   
    }

    create(){
        // background
        let bg = this.add.sprite(0, 0, 'background');
        bg.setOrigin(0,0);

        //player
        this.player = this.add.sprite(40, this.sys.game.config.height / 2, 'player');
        this.isPlayerAlive = true;    
        // scale down
        this.player.setScale(0.5);

        //get keys
        this.cursorKeys = this.input.keyboard.createCursorKeys();

        // goal
        this.treasure = this.add.sprite(this.sys.game.config.width - 80, this.sys.game.config.height / 2, 'treasure');
        this.treasure.setScale(0.6);

        //dragons
        // group of enemies
        this.enemies = this.add.group({
            key: 'dragon',
            repeat: 5,
            setXY: {
            x: 110,
            y: 100,
            stepX: 80,
            stepY: 20
            }
        });
        // scale enemies
        Phaser.Actions.ScaleXY(this.enemies.getChildren(), -0.5, -0.5);
        // set speeds
        Phaser.Actions.Call(this.enemies.getChildren(), function(enemy) {
            enemy.speed = Math.random() * 2 + 1;
        }, this);


    }

    update(){
        if (!this.isPlayerAlive) {
            return;
        }
        // check for active input
        if (this.cursorKeys.right.isDown) {
            // player walks
            this.player.x += this.playerSpeed;
        }

        // treasure collision
        if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), this.treasure.getBounds())) {
            this.gameOver();
        }

        //enemy movement
        // enemy movement
        let enemies = this.enemies.getChildren();
        let numEnemies = enemies.length;
        
        for (let i = 0; i < numEnemies; i++) {
        
            // move enemies
            enemies[i].y += enemies[i].speed;
        
            // reverse movement if reached the edges
            if (enemies[i].y >= this.enemyMaxY && enemies[i].speed > 0) {
            enemies[i].speed *= -1;
            } else if (enemies[i].y <= this.enemyMinY && enemies[i].speed < 0) {
            enemies[i].speed *= -1;
            }

             // enemy collision
            if (Phaser.Geom.Intersects.RectangleToRectangle(this.player.getBounds(), enemies[i].getBounds())) {
                this.gameOver();
                break;
            }
        }

       
    }

    // end the game
    gameOver(){
         // flag to set player is dead
        this.isPlayerAlive = false;

        // shake the camera
        this.cameras.main.shake(500);

         // fade camera
        this.time.delayedCall(250, function() {
            this.cameras.main.fade(250);
        }, [], this);
        
        
        // restart game
        this.time.delayedCall(500, function() {
            this.scene.restart();
        }, [], this);
    }

    render(){

    }

    shutdown(){

    }

    destroy() {

    }
}
