# Dodger Game
A Phaser 3 Example Game Written In ES6  
  
This is an example game coded in ES6 following a tutorial by Pablo Farias Navarro
on gamedevacademy.org (see https://gamedevacademy.org/phaser-3-tutorial/). 

Author: Nolan Sherman  
Organization: University of New Orleans Computer Science Department  

## Running this game
If you have python installed the simplest way to get this game running
is by creating a python http server. First enter a terminal and navigate 
to this projects directory.   

Then run: `python -m SimpleHTTPServer 8000`

